![TBZ Logo](../x_gitressourcen/tbz_logo.png)

---

# Zeichensätze

[TOC]

## ASCII (American Standard Code for Information Interchange)

[ASCII](https://www.asciitable.com/) bezeichnet ein Set von Zeichen und weist diesen feste Zahlen zu. 

![asciibasic](./images/asciibasic.gif)

Der Hintergrund ist die Art und Weise wie Daten digital gespeichert werden. Jedes Zeichen (Zahlen, Buchstaben, Zeilenumbrüche, etc) werden auf der Festplatte oder im Speicher binär gespeichert. 

**Fragen**:

- Wie speichert man das Zeichen '*A*' binär?
- Wie speichert man die Zahl '*65*' binär?

Wenn sie in der Tabelle oben nachschauen, finden sie in der Spalte 3, Zeile 2 die Antwort zu der Frage. Die Zahl *65* und das Zeichen *A* sind gleichwertig, resp. das Zeichen *A* kann durch die Zahl *65* repräsentiert werden, aber auch umgekehrt! Sowohl *A* als auch *65* werden mit der binären Repräsentation *01000001* gespeichert.

Durch Metadaten weiss ein Programm, ob es sich um ein Zeichen oder eine Zahl handelt, aber eigentlich ist das egal, denn es ist das gleiche. In einem Java-Programm (und auch allen anderen Programmen) können sie einen Datentyp in den anderen umwandeln:

- `char c = 'A'; int i = c; // Die Variable i hat den Wert 65`
- `int i = 65; char c = i; // Die Variable c hat den Wert 'A'`
- `char c = '1'; int i = c; // Die Variable i hat den Wert 49 und nicht 1. Schauen sie in der ASCII-Tabelle nach.`

## UTF-8 (Unicode)

Weil die ASCII-Tabelle mit 7-bit Speicherplatz arbeitet (also 7 Stellen mit 0 oder 1), reichen die Möglichkeiten nicht, um alle Zeichen abzubilden. Die Umlaute aus der Deutschen Sprache und die verschiedenen Zeichen aus der Französischen Sprache fehlen. 

[UTF-8](https://en.wikipedia.org/wiki/UTF-8) erweitert die 7-bit auf 8-bit und kann so die meisten Zeichen abbilden.  UTF_8 ist die Zeichencodierung, die meistens in den verschiedenen Programmen verwendet wird. 

**Der Nachteil**: Der Speicherplatz wird erhöht für jedes Zeichen

## Andere

Es gibt sehr viele Zeichensätze, die verschiedenen Bedürfnisse abbilden. Folgend zwei, denen sie zusätzlich begegnen könnten:

- [UTF-16](https://en.wikipedia.org/wiki/UTF-16): Enthält viel mehr Zeichen, auch historische Skript-Zeichen. Der Speicherplatz ist aber doppelt so hoch wie für UTF-8

- [ISO/IEC 8859-1 / Latin-1](ISO/IEC 8859-1 / Latin-1): Ein Zeichensatz der oft in Europa Verwendung findet, weil er all die lateinischen Zeichen abbildet, aber auch Umlaute. 

---

&copy;TBZ, 2022, Modul: m162