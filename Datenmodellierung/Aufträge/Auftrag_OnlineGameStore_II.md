![TBZ Logo](../../x_gitressourcen/tbz_logo.png)

---

# Auftrag "Online Game-Store II": Vom konzeptionellen zum logischen ERD

In dieser Übung wandeln sie ein konzeptionellen in das logische ERD um. 

#### Aufgabe

Schauen sie sich das folgende konzeptionelle ERD an. Wandeln sie es in ein logisches um.

![konzERD](x_gitressourcen/OnlineGameStore_II.png)

**Tasks**:

- Erstellen sie das ERD mit einem Tool (z.B. draw.io)
- lösen sie netzwerkförmige Kardinalitäten auf
- Fügen sie Primärschlüssel und Fremdschlüssel hinzu
- Fügen sie weitere Attribute hinzu
#### Zeit und Form

- 30 Minuten
- Individuell

---

&copy;TBZ, 2021, Modul: m162